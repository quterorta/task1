<?php

namespace ShippingPayment\ShippingModule\Model\Adminhtml\System\Config\Source\Customer;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Customer\Model\ResourceModel\Group\CollectionFactory;

/**
 * Customer Group options for select in admin page
 */
class Group extends AbstractSource
{
    /**
     * @var CollectionFactory
     */
    private $groupCollectionFactory;

    /**
     * @param CollectionFactory $groupCollectionFactory
     */
    public function __construct(
        CollectionFactory $groupCollectionFactory
    )
    {
        $this->groupCollectionFactory = $groupCollectionFactory;
    }

    /**
     * @return array|null
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = $this->groupCollectionFactory->create()->loadData()->toOptionArray();
        }
        return $this->_options;
    }
}
