<?php

namespace ShippingPayment\ShippingModule\Plugin\Model;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use ShippingPayment\ShippingModule\Model\Carrier\FreeShipping;
use Magento\Shipping\Model\Shipping;

/**
 * Plugin for show only free shipping method for specified customer group
 */
class ShippingMethodManagement {

    /** @var CheckoutSession */
    private $checkoutSession;
    /** @var FreeShipping */
    private $freeShipping;

    /**
     * @param CheckoutSession $checkoutSession
     * @param FreeShipping $freeShipping
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        FreeShipping $freeShipping
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->freeShipping = $freeShipping;
    }

    /**
     * @param Shipping $subject
     * @param $result
     * @return Shipping
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterCollectRates(Shipping $subject, $result)
    {
        $freeShippingCode = $this->freeShipping::CODE_CUSTOM_SHIPPING_PAYMENT_MODULE;
        $allowedCustomerGroupId = $this->freeShipping->getCustomerGroupIds();

        $customerGroupId = $this->checkoutSession->getQuote()->getCustomerGroupId();

        $result = $subject->getResult();
        $rates = $result->getAllRates();
        $result->reset();
        foreach ($rates as $rate) {
            $restrict = false;
            if (str_contains($allowedCustomerGroupId, $customerGroupId)) {
                if ($rate->getCarrier() != $freeShippingCode) {
                    $restrict = true;
                }
            } else {
                if ($rate->getCarrier() == $freeShippingCode) {
                    $restrict = true;
                }
            }
            if (!$restrict) {
                $result->append($rate);
            }
        }
        return $subject;
    }
}
