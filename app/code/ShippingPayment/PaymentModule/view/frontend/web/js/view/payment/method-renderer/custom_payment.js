define(
    [
        'Magento_Checkout/js/view/payment/default'
    ],
    function (Component) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'ShippingPayment_PaymentModule/payment/form',
            },

            /** Returns send check to info */
            getMailingAddress: function() {
                return window.checkoutConfig.payment.customPayment.mailingAddress;
            },

            getCode: function() {
                return 'customPayment';
            },
        });
    }
);
