define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'customPayment',
                component: 'ShippingPayment_PaymentModule/js/view/payment/method-renderer/custom_payment'
            }
        );
        /** Add view logic here if needed */
        return Component.extend({});
    }
);
