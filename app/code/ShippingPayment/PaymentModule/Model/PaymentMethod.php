<?php

namespace ShippingPayment\PaymentModule\Model;

use Magento\Payment\Model\Method\AbstractMethod;

class PaymentMethod extends AbstractMethod
{
    const METHOD_CODE = 'customPayment';
    /**
     * Payment method code
     *
     * @var string
     */
    protected $_code = self::METHOD_CODE;
    protected $_canAuthorize = true;
}
