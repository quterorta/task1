<?php

namespace ShippingPayment\PaymentModule\Model\Config\Source;

use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\Collection;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Framework\Exception\LocalizedException;

/**
 * Product category options for select in admin page
 */
class Category extends AbstractSource
{
    /** @var CategoryFactory  */
    private $categoryFactory;

    /** @var CollectionFactory  */
    private $categoryCollectionFactory;

    /**
     * @param CategoryFactory $categoryFactory
     * @param CollectionFactory $categoryCollectionFactory
     */
    public function __construct(
        CategoryFactory $categoryFactory,
        CollectionFactory $categoryCollectionFactory
    ) {
        $this->categoryFactory = $categoryFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * @return array|null
     * @throws LocalizedException
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = $this->getCategoriesList();
        }
        return $this->_options;
    }

    /**
     * @return array
     * @throws LocalizedException
     */
    public function getCategoriesList(): array
    {
        $categories = $this->getCategoryCollection(true, false, false, false);
        $categoryList = [];

        foreach ($categories as $category)
        {
            $categoryList[] = [
                'value' => $category->getEntityId(),
                'label' => $this->getParentName($category->getPath()) . $category->getName()
            ];
        }

        return $categoryList;
    }

    /**
     * @param bool $isActive
     * @param bool $level
     * @param bool $sortBy
     * @param bool $pageSize
     * @return Collection
     * @throws LocalizedException
     */
    public function getCategoryCollection($isActive = true, $level = false, $sortBy = false, $pageSize = false)
    {
        $collection = $this->categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');

        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }

        // select categories of certain level
        if ($level) {
            $collection->addLevelFilter($level);
        }

        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }

        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize);
        }

        return $collection;
    }

    /**
     * @param string $path
     * @return string
     */
    private function getParentName($path = '')
    {
        $parentName = '';
        $rootCats = array(1,2);

        $catTree = explode("/", $path);
        // Deleting category itself
        array_pop($catTree);

        if($catTree && (count($catTree) > count($rootCats)))
        {
            foreach ($catTree as $catId)
            {
                if(!in_array($catId, $rootCats))
                {
                    $category = $this->categoryFactory->create()->load($catId);
                    $categoryName = $category->getName();
                    $parentName .= $categoryName . ' | ';
                }
            }
        }

        return $parentName;
    }

}
