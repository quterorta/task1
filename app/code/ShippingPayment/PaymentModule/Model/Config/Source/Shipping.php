<?php

namespace ShippingPayment\PaymentModule\Model\Config\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;
use Magento\Shipping\Model\Config\Source\Allmethods;

/**
 * Shipping Methods options for select in admin page
 */
class Shipping extends AbstractSource
{
    /** @var Allmethods */
    private $shippingMethods;

    /**
     * @param Allmethods $shippingMethods
     */
    public function __construct(
        Allmethods $shippingMethods
    )
    {
        $this->shippingMethods = $shippingMethods;
    }

    /**
     * @return array|null
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = $this->shippingMethods->toOptionArray();
        }
        return $this->_options;
    }
}
