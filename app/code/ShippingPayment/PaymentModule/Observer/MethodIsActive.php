<?php

namespace ShippingPayment\PaymentModule\Observer;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Plugin for show/hide custom payment method
 */
class MethodIsActive implements ObserverInterface
{
    /**
     * @var Cart
     */
    protected $_cart;
    /**
     * @var Session
     */
    protected $_checkoutSession;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    /**
     * @param Cart $cart
     * @param Session $checkoutSession
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Cart $cart,
        Session $checkoutSession,
        ProductRepositoryInterface $productRepository
    )
    {
        $this->_cart = $cart;
        $this->_checkoutSession = $checkoutSession;
        $this->productRepository = $productRepository;
    }
    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $quote = $this->getCheckoutSession()->getQuote();

        $method = $observer->getEvent()->getMethodInstance();
        $allowedCategoryId = $method->getConfigData('product_category');

        $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
        $allowedShippingMethod = $method->getConfigData('shipping_method');

        $items = $quote->getAllItems();
        $flag = false;
        foreach ($items as $item) {
            $productId = $item->getProductId();
            $product = $this->getProduct($productId);

            $categoryIds = $product->getCategoryIds();
            if (in_array($allowedCategoryId, $categoryIds)) {
                $flag = true;
                break;
            }
        }

        if ($observer->getEvent()->getMethodInstance()->getCode() == "customPayment")
        {
           if ($flag == false || $shippingMethod !== $allowedShippingMethod) {
               $checkResult = $observer->getEvent()->getResult();
               $checkResult->setData('is_available', false);
           }
        }
    }

    /**
     * @param $productId
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    public function getProduct($productId)
    {
        return $this->productRepository->getById($productId);
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->_cart;
    }

    /**
     * @return Session
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}
