<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Learning\SecondUnit\Controller\Index;

use Magento\CatalogRule\Model\ResourceModel\Rule\Collection as CatalogRuleCollection;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\View\Result\PageFactory;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use DateTime;


class GetMaxDate implements HttpGetActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;
    /**
     * @var Json
     */
    protected $serializer;
    /**
     * @var LoggerInterface
     */
    protected $logger;
    /**
     * @var Http
     */
    protected $http;

    protected $catalogRuleCollection;

    /**
     * Constructor
     *
     * @param PageFactory $resultPageFactory
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Http $http
     */
    public function __construct(
        PageFactory $resultPageFactory,
        Json $json,
        LoggerInterface $logger,
        Http $http,
        CatalogRuleCollection $catalogRuleCollection,
        RequestInterface $req,
        ProductRepositoryInterface $productRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $json;
        $this->logger = $logger;
        $this->http = $http;
        $this->catalogRuleCollection = $catalogRuleCollection;
        $this->req = $req;
        $this->productRepository = $productRepository;
    }

    /**
     * Execute view action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $data = $this->getMaxDate();

        try {
            return $this->jsonResponse($data);
        } catch (LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return ResultInterface
     */
    public function jsonResponse($response = '')
    {
        $this->http->getHeaders()->clearHeaders();
        $this->http->setHeader('Content-Type', 'application/json');
        return $this->http->setBody(
            $this->serializer->serialize($response)
        );
    }

    public function getMaxDate() {

        $productId = $this->req->getParam('pid');
        $product = $this->productRepository->getById($productId);

        $specialDate = $product->getSpecialToDate();

        $collection = $this->catalogRuleCollection;
        $rulesDates = [];
        foreach ($collection as $item) {
            if(array_key_exists($productId, $item->getMatchingProductIds())) {
                $expiredDate = $item->getToDate();
                $rulesDates[] = $expiredDate;
            }
        }
        $ruleDate = max($rulesDates);

        if ($specialDate > $ruleDate) {
            $callbackDate = new DateTime($specialDate);
        } else {
            $callbackDate = new DateTime($ruleDate);
        }

        $callbackDate = $callbackDate->format('Y/m/d');

        return $callbackDate;
    }

}
