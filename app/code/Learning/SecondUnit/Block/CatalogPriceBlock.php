<?php

namespace Learning\SecondUnit\Block;

use Magento\CatalogRule\Model\Rule;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject;
use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Model\ProductRepository;
use Magento\CatalogRule\Model\ResourceModel\Rule\Collection as CatalogRuleCollection;
use Magento\Framework\Registry;
use Safe\DateTime;

class CatalogPriceBlock extends \Magento\Framework\View\Element\Template
{
    private $_productRepository;
    private $_registry;
    /** @var CatalogRuleCollection */
    private $catalogRuleCollection;

    public function __construct (
        Context $context,
        ProductRepository $productRepository,
        CatalogRuleCollection $catalogRuleCollection,
        Registry $registry,
        array $data = []
    ) {
        $this->_productRepository = $productRepository;
        $this->_registry = $registry;
        $this->catalogRuleCollection = $catalogRuleCollection;
        parent::__construct($context, $data);
    }

    public function getCurrentProduct() {
        return $this->_registry->registry('current_product');
    }

    public function getSpecialDate() {
        return $this->getCurrentProduct()->getSpecialToDate();
    }

    public function getCatalogRulesDates() {
        $productId = $this->getCurrentProduct()->getId();
        $collection = $this->catalogRuleCollection;
        $dates = [];
        /** @var Rule $item */
        foreach ($collection as $item) {
            if(array_key_exists($productId, $item->getMatchingProductIds())) {
                $expiredDate = $item->getToDate();
                $dates[] = $expiredDate;
            }
        }
        return $dates;
    }

}
