<?php
namespace Learning\ThirdUnit\Controller\Index;

use Magento\Catalog\Model\ResourceModel\Product\Collection as ProductCollection;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    )
    {
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $products = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');

        foreach ($products as $product) {
            $product_categories = $product->getCategoryIds();
            foreach ($product_categories as $product_category) {
                echo 'ID product: '.$product->getId().', ID category: '.$product_category.'<br>';
            }
        }

    }
}
