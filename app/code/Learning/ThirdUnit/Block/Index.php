<?php

namespace Learning\ThirdUnit\Block;


class Index extends \Magento\Framework\View\Element\Template
{


    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Learning\ThirdUnit\Model\PostFactory $postFactory
    )
    {
        parent::__construct($context);
        $this->_postFactory = $postFactory;
    }

    public function getResult()
    {
        $post = $this->_postFactory->create();
        $collection = $post->getCollection();
        return $collection;
    }
}
