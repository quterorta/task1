<?php
namespace Learning\ThirdUnit\Model;
class Post extends \Magento\Framework\Model\AbstractModel
{


    protected function _construct()
    {
        $this->_init('Learning\ThirdUnit\Model\ResourceModel\Post');
    }
}
