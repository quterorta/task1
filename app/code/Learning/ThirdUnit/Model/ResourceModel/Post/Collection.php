<?php
namespace Learning\ThirdUnit\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Learning\ThirdUnit\Model\Post', 'Learning\ThirdUnit\Model\ResourceModel\Post');
    }

}
