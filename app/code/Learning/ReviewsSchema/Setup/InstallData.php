<?php

namespace Learning\ReviewsSchema\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Learning\ReviewsSchema\Model\ProductReviewFactory;

use DateTime;

class InstallData implements InstallDataInterface
{
    private $productReviewFactory;

    public function __construct(ProductReviewFactory $productReviewFactory)
    {
        $this->productReviewFactory = $productReviewFactory;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $data = [
            'category_id'       => 3,
            'product_id'        => 1,
            'text_review'       => 'Test text review',
            'name'              => 'Test user name for review',
            'email'             => 'test@email.com',
        ];
        $productReview = $this->productReviewFactory->create();
        $productReview->addData($data)->save();
    }
}
