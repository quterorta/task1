<?php

namespace Learning\ReviewsSchema\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Learning\ReviewsSchema\Model\ProductReviewFactory;
use DateTime;

class UpgradeData implements UpgradeDataInterface
{
    protected $productReviewFactory;

    public function __construct(ProductReviewFactory $productReviewFactory)
    {
        $this->productReviewFactory = $productReviewFactory;
    }

    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.3.0', '<')) {

            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $products = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');

            foreach ($products as $product) {
                $product_categories = $product->getCategoryIds();
                foreach ($product_categories as $product_category) {
                    $productId = $product->getId();
                    $data = [
                        'category_id'       => $product_category,
                        'product_id'        => $productId,
                        'text_review'       => 'Text review for product with ID: '.$productId.' and category ID: '.$product_category.'.',
                        'name'              => 'username',
                        'email'             => 'useremail@email.com',
                        'creation_time'        => new DateTime(),
                        'update_time'        => new DateTime()
                    ];
                    $productReview = $this->productReviewFactory->create();
                    $productReview->addData($data)->save();
                }
            }
        }
    }
}
