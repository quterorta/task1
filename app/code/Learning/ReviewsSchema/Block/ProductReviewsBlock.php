<?php

namespace Learning\ReviewsSchema\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Framework\Registry;
use Learning\ReviewsSchema\Model\ResourceModel\ProductReview\Collection as ProductReviewCollection;

class ProductReviewsBlock extends Template
{

    public function __construct (
        Context $context,
        Registry $registry,
        ProductReviewCollection $productReviewCollection,
        array $data = []
    ) {
        $this->registry = $registry;
        $this->productReviewCollection = $productReviewCollection;
        parent::__construct($context, $data);
    }

    public function getCurrentProduct() {
        return $this->registry->registry('current_product');
    }

    public function getCurrentProductId() {
        return $this->registry->registry('current_product')->getId();
    }

    public function getCurrentProductTitle() {
        return $this->registry->registry('current_product')->getName();
    }

    public function getProductReviews() {
        //return $this->productReviewCollection;
        $productId = $this->getCurrentProductId();
        return $this->productReviewCollection->getByProductId($productId);
    }




}
