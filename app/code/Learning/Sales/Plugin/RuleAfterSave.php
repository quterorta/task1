<?php

namespace Learning\Sales\Plugin;

use Exception;
use Learning\Sales\Helper\Data;
use Learning\Sales\Block\SalesBlock;
use Magento\Config\Model\Config;
use Magento\SalesRule\Model\RuleFactory;
use Magento\SalesRule\Model\Rule;

class RuleAfterSave
{
    /** @var RuleFactory */
    private $ruleFactory;

    /** @var Data */
    private $helperConfigData;

    /** @var SalesBlock */
    private $salesBlock;

    /**
     * @param RuleFactory $ruleFactory
     * @param Data $helperConfigData
     * @param SalesBlock $salesBlock
     */
    public function __construct(
        RuleFactory $ruleFactory,
        Data $helperConfigData,
        SalesBlock $salesBlock
    ) {
        $this->ruleFactory = $ruleFactory;
        $this->helperConfigData = $helperConfigData;
        $this->salesBlock = $salesBlock;
    }


    /**
     * @param Config $config
     * @param $result
     * @return mixed
     * @throws Exception
     */
    public function afterSave(Config $config, $result)
    {
        $salesRule = $this->ruleFactory->create();

        $customerGroupsForRule = $this->helperConfigData->getCustomerGroupList();
        $discountAmount = $this->helperConfigData->getGeneralConfig('discount_percent');
        $discountFromDate = $this->salesBlock->getCurrentDate();
        $discountToDate = $this->helperConfigData->getGeneralConfig('discount_end_date');

        if (
            $this->salesBlock->checkEnableModule() != 0 &&
            $this->salesBlock->checkEnableDiscount() != 0 &&
            $this->salesBlock->getCurrentDate() <= $this->salesBlock->getDiscountEndDate() &&
            $this->salesBlock->getCurrentTime() > $this->salesBlock->getDiscountFromTime() &&
            $this->salesBlock->getCurrentTime() < $this->salesBlock->getDiscountToTime()
        ){
            $salesRule->setData([
                'name' => 'Rule for learning sale',
                'description' => 'some rule description',
                'from_date' => $discountFromDate,
                'to_date' => $discountToDate,
                'is_active' => 1,
                'customer_group_ids' => $customerGroupsForRule,
                'coupon_type' => Rule::COUPON_TYPE_NO_COUPON,
                'simple_action' => Rule::BY_PERCENT_ACTION,
                'discount_amount' => $discountAmount,
                'discount_step' => 0,
                'stop_rules_processing' => 0,
                'website_ids' => [1]
            ]);
        }

        $salesRule->save();

        return $result;
    }
}
