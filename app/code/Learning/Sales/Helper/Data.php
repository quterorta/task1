<?php

namespace Learning\Sales\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const XML_PATH_SALES = 'sales_admin/';

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_SALES . 'general/' . $code, $storeId);
    }

    public function getCustomerGroupList()
    {
        $list = $this->scopeConfig->getValue("sales_admin/general/discount_customer_group_list", ScopeInterface::SCOPE_STORE);
        return $list !== null ? explode(',', $list) : [];
    }
}
