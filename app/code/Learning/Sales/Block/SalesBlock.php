<?php

namespace Learning\Sales\Block;

use Learning\Sales\Helper\Data;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Session as CatalogSession;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Exception\LocalizedException;
use DateTime;

class SalesBlock extends Template
{
    /*** @var CatalogSession */
    private $catalogSession;
    /*** @var ProductRepositoryInterface*/
    private $productRepository;
    /*** @var ProductInterface */
    private $currentProduct;
    /*** @var CustomerSession */
    private $customerSession;
    /*** @var GroupRepositoryInterface */
    private $groupRepository;
    /*** @var  TimezoneInterface */
    private $timezone;

    /**
     * @param CatalogSession $catalogSession
     * @param ProductRepositoryInterface $productRepository
     * @param Data $helperData
     * @param CustomerSession $customerSession
     * @param GroupRepositoryInterface $groupRepository
     * @param TimezoneInterface $timezone
     */
    public function __construct (
        Context $context,
        Data $helperData,
        CatalogSession $catalogSession,
        ProductRepositoryInterface $productRepository,
        CustomerSession $customerSession,
        GroupRepositoryInterface $groupRepository,
        TimezoneInterface $timezone,
        array $data = []
    ) {
        $this->catalogSession = $catalogSession;
        $this->productRepository = $productRepository;
        $this->helperData = $helperData;
        $this->customerSession = $customerSession;
        $this->groupRepository = $groupRepository;
        $this->timezone = $timezone;
        parent::__construct($context, $data);
    }

    /**
     * @return ProductInterface
     * @throws NoSuchEntityException
     */
    public function getProduct()
    {
        if (!isset($this->currentProduct)) {
            $productId = $this->getProductId();

            if ($productId) {
                $this->currentProduct = $this->productRepository->getById($productId);
            }
        }
        return $this->currentProduct;
    }

    /**
     * @return mixed
     */
    public function getProductId()
    {
        return $this->catalogSession->getData('last_viewed_product_id');
    }

    /**
     * @return mixed
     */
    public function checkEnableModule() {
        return $this->helperData->getGeneralConfig('enable');
    }

    /**
     * @return mixed
     */
    public function checkEnableDiscount() {
        return $this->helperData->getGeneralConfig('discount_enable');
    }

    /**
     * @return string
     */
    public function getCurrentTime() {
        $currentDateTime = new DateTime();
        return $currentDateTime->format('H,i,s');
    }

    /**
     * @return string
     */
    public function getCurrentDate() {
        return $this->timezone->date()->format('Y-m-d');
    }

    /**
     * @return mixed
     */
    public function getDiscountPercent() {
        return $this->helperData->getGeneralConfig('discount_percent');
    }

    /**
     * @return mixed
     */
    public function getDiscountFromTime() {
        return $this->helperData->getGeneralConfig('discount_time_from');
    }

    /**
     * @return mixed
     */
    public function getDiscountToTime() {
        return $this->helperData->getGeneralConfig('discount_time_to');
    }

    /**
     * @return mixed
     */
    public function getDiscountEndDate() {
        return $this->helperData->getGeneralConfig('discount_end_date');
    }

    /**
     * @return array|false|string[]
     */
    public function getCustomerGroups() {
        return $this->helperData->getCustomerGroupList();
    }

    /**
     * @return int
     */
    public function checkCurrentCustomerGroup() {
        if($this->customerSession->isLoggedIn()) {
            return $this->customerSession->getCustomer()->getGroupId();
        } else {
            return 0;
        }
    }

    /**
     * @return float|null
     * @throws NoSuchEntityException
     */
    public function getProductPrice() {
        return $this->getProduct()->getPrice();
    }

    /**
     * @return float|int|null
     * @throws NoSuchEntityException
     */
    public function getNewPrice() {
        $productPrice = $this->getProductPrice();
        $discountPrice = $productPrice*$this->getDiscountPercent()/100;
        return $productPrice-$discountPrice;
    }

    /**
     * @return string|null
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function getCustomerGroupName() {
        $currentCustomerGroupId = $this->checkCurrentCustomerGroup();
        $discountCustomerGroups = $this->getCustomerGroups();
        if(in_array($currentCustomerGroupId, $discountCustomerGroups)) {
            return $this->groupRepository->getById($currentCustomerGroupId)->getCode();
        } else {
            return null;
        }
    }

    /**
     * @return bool
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function getAvailableDiscount() {
        if ($this->checkEnableModule() != 0) {
            if ($this->checkEnableDiscount() != 0) {
                if ($this->getCustomerGroupName() !== null) {
                    if ($this->getCurrentDate() <= $this->getDiscountEndDate()) {
                        if ($this->getCurrentTime() > $this->getDiscountFromTime()) {
                            if ($this->getCurrentTime() < $this->getDiscountToTime()) {
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

}

