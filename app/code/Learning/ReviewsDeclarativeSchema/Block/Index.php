<?php

namespace Learning\ReviewsDeclarativeSchema\Block;

use Magento\Framework\View\Element\Template\Context;
use Learning\ReviewsDeclarativeSchema\Model\ProductDeclarativeReviewFactory;
use Magento\Catalog\Model\ProductRepository;
use Magento\Catalog\Helper\Image;
use Magento\Store\Model\StoreManagerInterface;
use DateTime;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $productRepository;
    protected $imageHelper;
    protected $storeManager;

    public function __construct(
        Context $context,
        ProductDeclarativeReviewFactory $productDeclarativeReviewFactory,
        ProductRepository $productRepository,
        Image $imageHelper,
        StoreManagerInterface $storeManager
    )
    {
        parent::__construct($context);
        $this->productDeclarativeReviewFactory = $productDeclarativeReviewFactory;
        $this->productRepository = $productRepository;
        $this->imageHelper = $imageHelper;
        $this->storeManager =  $storeManager;
    }

    public function setSearcingCategory() {
        $categoryId = 3;
        return $categoryId;
    }

    public function setSearchingDate() {
        $date = date('d/m/Y');
        return $date;
    }

    public function getFilter() {
        $review = $this->productDeclarativeReviewFactory->create();
        $collection = $review->getCollection();
        $collection->addFieldToFilter('category_id', 3)->addFieldToFilter('creation_time', array('gteq' => date('Y-m-01')));
        return $collection;
    }

    public function getProductById($productId) {
        return $this->productRepository->getById($productId);
    }

    public function getProductImageUrl($product) {

        $store = $this->storeManager->getStore();
        $productImageUrl = $store->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'catalog/product' .$product->getImage();
        return $productImageUrl;
    }

}
