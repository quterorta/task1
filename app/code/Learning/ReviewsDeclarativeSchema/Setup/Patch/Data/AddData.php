<?php
namespace Learning\ReviewsDeclarativeSchema\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchVersionInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Learning\ReviewsDeclarativeSchema\Model\ProductDeclarativeReviewFactory;
Use Learning\ReviewsDeclarativeSchema\Model\ResourceModel\ProductDeclarativeReview;
use DateTime;

class AddData implements DataPatchInterface, PatchVersionInterface
{
    private $productDeclarativeReviewFactory;
    private $productDeclarativeReviewResource;
    private $moduleDataSetup;
    public function __construct(
        ProductDeclarativeReviewFactory $productDeclarativeReviewFactory,
        ProductDeclarativeReview $productDeclarativeReviewResource,
        ModuleDataSetupInterface $moduleDataSetup
    )
    {
        $this->productDeclarativeReviewFactory = $productDeclarativeReviewFactory;
        $this->productDeclarativeReviewResource = $productDeclarativeReviewResource;
        $this->moduleDataSetup=$moduleDataSetup;
    }

    public function apply()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $products = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');

        //Install data row into contact_details table
        $this->moduleDataSetup->startSetup();

        foreach ($products as $product) {
            $product_categories = $product->getCategoryIds();
            foreach ($product_categories as $product_category) {
                $productId = $product->getId();
                $productReview = $this->productDeclarativeReviewFactory->create();
                $productReview->setCategoryId($product_category)
                    ->setProductId($productId)
                    ->setTextReview('Text review for product with ID: ' . $productId . ' and category ID: ' . $product_category . ' (Declarative schema).')
                    ->setName('username')
                    ->setEmail('user_email@mail.com')
                    ->setCreationTime(new DateTime())
                    ->setUpdateTime(new DateTime());
                $this->productDeclarativeReviewResource->save($productReview);
            }
        }

        $this->moduleDataSetup->endSetup();
    }
    public static function getDependencies()
    {
        return [];
    }
    public static function getVersion()
    {
        return '1.1.1';
    }
    public function getAliases()
    {
        return [];
    }
}
