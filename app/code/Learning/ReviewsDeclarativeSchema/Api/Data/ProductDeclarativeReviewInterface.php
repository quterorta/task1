<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Learning\ReviewsDeclarativeSchema\Api\Data;

interface ProductDeclarativeReviewInterface
{

    const CATEGORY_ID = 'category_id';
    const TEXT_REVIEW = 'text_review';
    const ID = 'id';
    const NAME = 'name';
    const PRODUCTDECLARATIVEREVIEW_ID = 'productdeclarativereview_id';
    const PRODUCT_ID = 'product_id';
    const UPDATE_TIME = 'update_time';
    const CREATION_TIME = 'creation_time';
    const EMAIL = 'email';

    /**
     * Get productdeclarativereview_id
     * @return string|null
     */
    public function getProductdeclarativereviewId();

    /**
     * Set productdeclarativereview_id
     * @param string $productdeclarativereviewId
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setProductdeclarativereviewId($productdeclarativereviewId);

    /**
     * Get id
     * @return string|null
     */
    public function getId();

    /**
     * Set id
     * @param string $id
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setId($id);

    /**
     * Get category_id
     * @return string|null
     */
    public function getCategoryId();

    /**
     * Set category_id
     * @param string $categoryId
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setCategoryId($categoryId);

    /**
     * Get product_id
     * @return string|null
     */
    public function getProductId();

    /**
     * Set product_id
     * @param string $productId
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setProductId($productId);

    /**
     * Get text_review
     * @return string|null
     */
    public function getTextReview();

    /**
     * Set text_review
     * @param string $textReview
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setTextReview($textReview);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setName($name);

    /**
     * Get email
     * @return string|null
     */
    public function getEmail();

    /**
     * Set email
     * @param string $email
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setEmail($email);

    /**
     * Get creation_time
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Set creation_time
     * @param string $creationTime
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setCreationTime($creationTime);

    /**
     * Get update_time
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Set update_time
     * @param string $updateTime
     * @return \Learning\ReviewsDeclarativeSchema\ProductDeclarativeReview\Api\Data\ProductDeclarativeReviewInterface
     */
    public function setUpdateTime($updateTime);
}

