<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Learning\ReviewsDeclarativeSchema\Api\Data;

interface ProductDeclarativeReviewSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get ProductDeclarativeReview list.
     * @return \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface[]
     */
    public function getItems();

    /**
     * Set id list.
     * @param \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

