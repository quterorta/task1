<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Learning\ReviewsDeclarativeSchema\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ProductDeclarativeReviewRepositoryInterface
{

    /**
     * Save ProductDeclarativeReview
     * @param \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface $productDeclarativeReview
     * @return \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface $productDeclarativeReview
    );

    /**
     * Retrieve ProductDeclarativeReview
     * @param string $productdeclarativereviewId
     * @return \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($productdeclarativereviewId);

    /**
     * Retrieve ProductDeclarativeReview matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete ProductDeclarativeReview
     * @param \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface $productDeclarativeReview
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface $productDeclarativeReview
    );

    /**
     * Delete ProductDeclarativeReview by ID
     * @param string $productdeclarativereviewId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($productdeclarativereviewId);
}

