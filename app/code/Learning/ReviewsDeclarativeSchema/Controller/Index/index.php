<?php
namespace Learning\ReviewsDeclarativeSchema\Controller\Index;

use Learning\ReviewsDeclarativeSchema\Model\ResourceModel\ProductDeclarativeReview\Collection as ReviewCollection;

class index extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        ReviewCollection $reviewsCollection
    )
    {
        $this->_pageFactory = $pageFactory;
        $this->reviewsCollection = $reviewsCollection;
        return parent::__construct($context);
    }

    public function execute()
    {
        return $this->_pageFactory->create();
    }
}
