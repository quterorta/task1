<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Learning\ReviewsDeclarativeSchema\Model;

use Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface;
use Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterfaceFactory;
use Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewSearchResultsInterfaceFactory;
use Learning\ReviewsDeclarativeSchema\Api\ProductDeclarativeReviewRepositoryInterface;
use Learning\ReviewsDeclarativeSchema\Model\ResourceModel\ProductDeclarativeReview as ResourceProductDeclarativeReview;
use Learning\ReviewsDeclarativeSchema\Model\ResourceModel\ProductDeclarativeReview\CollectionFactory as ProductDeclarativeReviewCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class ProductDeclarativeReviewRepository implements ProductDeclarativeReviewRepositoryInterface
{

    /**
     * @var ProductDeclarativeReviewInterfaceFactory
     */
    protected $productDeclarativeReviewFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var ResourceProductDeclarativeReview
     */
    protected $resource;

    /**
     * @var ProductDeclarativeReview
     */
    protected $searchResultsFactory;

    /**
     * @var ProductDeclarativeReviewCollectionFactory
     */
    protected $productDeclarativeReviewCollectionFactory;


    /**
     * @param ResourceProductDeclarativeReview $resource
     * @param ProductDeclarativeReviewInterfaceFactory $productDeclarativeReviewFactory
     * @param ProductDeclarativeReviewCollectionFactory $productDeclarativeReviewCollectionFactory
     * @param ProductDeclarativeReviewSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceProductDeclarativeReview $resource,
        ProductDeclarativeReviewInterfaceFactory $productDeclarativeReviewFactory,
        ProductDeclarativeReviewCollectionFactory $productDeclarativeReviewCollectionFactory,
        ProductDeclarativeReviewSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->productDeclarativeReviewFactory = $productDeclarativeReviewFactory;
        $this->productDeclarativeReviewCollectionFactory = $productDeclarativeReviewCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(
        ProductDeclarativeReviewInterface $productDeclarativeReview
    ) {
        try {
            $this->resource->save($productDeclarativeReview);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the productDeclarativeReview: %1',
                $exception->getMessage()
            ));
        }
        return $productDeclarativeReview;
    }

    /**
     * @inheritDoc
     */
    public function get($productDeclarativeReviewId)
    {
        $productDeclarativeReview = $this->productDeclarativeReviewFactory->create();
        $this->resource->load($productDeclarativeReview, $productDeclarativeReviewId);
        if (!$productDeclarativeReview->getId()) {
            throw new NoSuchEntityException(__('ProductDeclarativeReview with id "%1" does not exist.', $productDeclarativeReviewId));
        }
        return $productDeclarativeReview;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->productDeclarativeReviewCollectionFactory->create();
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(
        ProductDeclarativeReviewInterface $productDeclarativeReview
    ) {
        try {
            $productDeclarativeReviewModel = $this->productDeclarativeReviewFactory->create();
            $this->resource->load($productDeclarativeReviewModel, $productDeclarativeReview->getProductdeclarativereviewId());
            $this->resource->delete($productDeclarativeReviewModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the ProductDeclarativeReview: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($productDeclarativeReviewId)
    {
        return $this->delete($this->get($productDeclarativeReviewId));
    }
}

