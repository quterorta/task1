<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Learning\ReviewsDeclarativeSchema\Model;

use Learning\ReviewsDeclarativeSchema\Api\Data\ProductDeclarativeReviewInterface;
use Magento\Framework\Model\AbstractModel;

class ProductDeclarativeReview extends AbstractModel implements ProductDeclarativeReviewInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(\Learning\ReviewsDeclarativeSchema\Model\ResourceModel\ProductDeclarativeReview::class);
    }

    /**
     * @inheritDoc
     */
    public function getProductdeclarativereviewId()
    {
        return $this->getDataData(self::PRODUCTDECLARATIVEREVIEW_ID);
    }

    /**
     * @inheritDoc
     */
    public function setProductdeclarativereviewId($productdeclarativereviewId)
    {
        return $this->setData(self::PRODUCTDECLARATIVEREVIEW_ID, $productdeclarativereviewId);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->getData(self::ID);
    }

    /**
     * @inheritDoc
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * @inheritDoc
     */
    public function getCategoryId()
    {
        return $this->getData(self::CATEGORY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setCategoryId($categoryId)
    {
        return $this->setData(self::CATEGORY_ID, $categoryId);
    }

    /**
     * @inheritDoc
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * @inheritDoc
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * @inheritDoc
     */
    public function getTextReview()
    {
        return $this->getData(self::TEXT_REVIEW);
    }

    /**
     * @inheritDoc
     */
    public function setTextReview($textReview)
    {
        return $this->setData(self::TEXT_REVIEW, $textReview);
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * @inheritDoc
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * @inheritDoc
     */
    public function getEmail()
    {
        return $this->getData(self::EMAIL);
    }

    /**
     * @inheritDoc
     */
    public function setEmail($email)
    {
        return $this->setData(self::EMAIL, $email);
    }

    /**
     * @inheritDoc
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * @inheritDoc
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * @inheritDoc
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * @inheritDoc
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }
}

