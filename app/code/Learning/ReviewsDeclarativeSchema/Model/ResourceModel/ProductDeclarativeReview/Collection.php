<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Learning\ReviewsDeclarativeSchema\Model\ResourceModel\ProductDeclarativeReview;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'productdeclarativereview_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \Learning\ReviewsDeclarativeSchema\Model\ProductDeclarativeReview::class,
            \Learning\ReviewsDeclarativeSchema\Model\ResourceModel\ProductDeclarativeReview::class
        );
    }
}

