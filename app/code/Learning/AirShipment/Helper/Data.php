<?php

namespace Learning\AirShipment\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const XML_PATH_AIRSHIPMENT = 'air_shipment_admin/';

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field, ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_AIRSHIPMENT .'general/'. $code, $storeId);
    }

    public function getAirShipmentParams($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_AIRSHIPMENT .'air_shipment_params/'. $code, $storeId);
    }

}
