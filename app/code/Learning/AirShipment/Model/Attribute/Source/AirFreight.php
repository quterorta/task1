<?php

namespace Learning\AirShipment\Model\Attribute\Source;

use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

class AirFreight extends AbstractSource
{
    /**
     * Get all options
     * @return array
     */
    public function getAllOptions()
    {
        if (!$this->_options) {
            $this->_options = [
                ['label' => __('None'), 'value' => 'none'],
                ['label' => __('Balloon'), 'value' => 'balloon'],
                ['label' => __('Charter plane'), 'value' => 'charter plane'],
                ['label' => __('High-speed plane'), 'value' => 'high-speed plane'],
            ];
        }
        return $this->_options;
    }
}
