<?php

namespace Learning\AirShipment\Block;

use Learning\AirShipment\Helper\Data;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Session as CatalogSession;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Catalog\Model\Product\Attribute\Repository as AttrRepository;

class AirShipmentBlock extends Template
{

    /** @var AttrRepository */
    private $attrRepository;

    /*** @var CatalogSession */
    private $catalogSession;

    /*** @var ProductRepositoryInterface*/
    private $productRepository;

    /*** Current Product @var ProductInterface */
    private $currentProduct;

    /**
     * @return ProductInterface
     * @throws NoSuchEntityException
     */

    /**
     * @param CatalogSession $catalogSession
     * @param ProductRepositoryInterface $productRepository
     * @param Data $helperData
     */
    public function __construct (
        Context $context,
        Data $helperData,
        CatalogSession $catalogSession,
        ProductRepositoryInterface $productRepository,
        AttrRepository $attrRepository,
        array $data = []
    ) {
        $this->catalogSession = $catalogSession;
        $this->productRepository = $productRepository;
        $this->helperData = $helperData;
        $this->attrRepository =$attrRepository;
        parent::__construct($context, $data);
    }

    public function getProduct(): ProductInterface
    {
        if (!isset($this->currentProduct)) {
            $productId = $this->getProductId();

            if ($productId) {
                $this->currentProduct = $this->productRepository->getById($productId);
            }
        }
        return $this->currentProduct;
    }

    /**
     * @return string
     */
    public function getProductId(): string
    {
        return $this->catalogSession->getData('last_viewed_product_id');
    }

    public function checkEnableModule() {
        return $this->helperData->getGeneralConfig('enable');
    }


    public function getAllBrand(){
        $manufacturerOptions = $this->attrRepository->get('air_freight')->getOptions();
        $values = array();
        foreach ($manufacturerOptions as $manufacturerOption) {
            //$manufacturerOption->getValue();  // Value
            $values[] = $manufacturerOption->getLabel();  // Label
        }
        return $values;
    }

    public function getMaxWeight() {
        $productAirShipmentType = $this->getProduct($this->getProductId())->getData('air_freight');
        if ($productAirShipmentType == 'balloon') {
            $max_weight = $this->helperData->getAirShipmentParams('air_shipment_balloon/balloon_max_weight');
        } else if ($productAirShipmentType == 'charter plane') {
            $max_weight = $this->helperData->getAirShipmentParams('air_shipment_charter_plane/charter_plane_max_weight');
        } else if ($productAirShipmentType == 'high-speed plane') {
            $max_weight = $this->helperData->getAirShipmentParams('high_speed_plane/high_speed_plane_max_weight');
        }
        return $max_weight;
    }


    public function getAdditionalPrice() {
        $productAirShipmentType = $this->getProduct($this->getProductId())->getData('air_freight');
        if ($productAirShipmentType == 'balloon') {
            $additionalPrice = $this->helperData->getAirShipmentParams('air_shipment_balloon/balloon_add_price');
        } else if ($productAirShipmentType == 'charter plane') {
            $additionalPrice = $this->helperData->getAirShipmentParams('air_shipment_charter_plane/charter_plane_add_price');
        } else if ($productAirShipmentType == 'high-speed plane') {
            $additionalPrice = $this->helperData->getAirShipmentParams('high_speed_plane/high_speed_plane_add_price');
        }
        return $additionalPrice;
    }

    public function checkDiffWeight() {
        $productWeight = $this->getProduct($this->getProductId())->getWeight();
        $maxShipmentWeight = $this->getMaxWeight();
        $diffWeight = abs($productWeight-$maxShipmentWeight);

        if ($diffWeight != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getShipmentPrice() {
        $productWeight = $this->getProduct($this->getProductId())->getWeight();
        $maxShipmentWeight = $this->getMaxWeight();
        $additionalShipmentPrice = $this->getAdditionalPrice();

        $diffWeight = abs($productWeight-$maxShipmentWeight);
        $additionalPrice = $additionalShipmentPrice*$diffWeight;
        return $additionalPrice;
    }

    public function getFinalShipmentPrice() {
        return $this->getProduct($this->getProductId())->getPrice() + $this->getShipmentPrice();
    }




}

