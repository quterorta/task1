<?php

namespace Shipping\International\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $installer->getConnection()->addColumn(
            $installer->getTable('quote'),
            'return_address',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'size' => 255,
                'comment' => 'Return Address',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order'),
            'return_address',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'size' => 255,
                'comment' => 'Return Address',
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('sales_order_grid'),
            'return_address',
            [
                'type' => Table::TYPE_TEXT,
                'nullable' => false,
                'size' => 255,
                'comment' => 'Return Address',
            ]
        );

        $setup->endSetup();
    }
}
