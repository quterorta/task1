var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/action/select-shipping-method': {
                'Shipping_International/js/mixin/select-shipping-method-mixin': true
            },
            'Magento_Checkout/js/model/checkout-data-resolver': {
                'Shipping_International/js/mixin/checkout-data-resolver-mixin': true
            },
            'Magento_Checkout/js/action/set-shipping-information': {
                'Shipping_International/js/action/set-shipping-information-mixin': true
            }
        }
    }
};
