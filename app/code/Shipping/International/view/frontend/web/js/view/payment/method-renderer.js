define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'shipping_international_credit_card_payment',
                component: 'Shipping_International/js/view/payment/method-renderer/credit-card-payment-method'
            }
        );
        return Component.extend({});
    }
);
