define(
    [
        'Magento_Customer/js/model/address-list',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/checkout-data',
        'Magento_Checkout/js/action/create-shipping-address',
        'Magento_Checkout/js/action/select-shipping-address',
        'Magento_Checkout/js/action/select-shipping-method',
        'Magento_Checkout/js/model/payment-service',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Checkout/js/model/address-converter',
        'Magento_Checkout/js/action/select-billing-address',
        'Magento_Checkout/js/action/create-billing-address',
        'underscore'
    ],
    function (
        addressList,
        quote,
        checkoutData,
        createShippingAddress,
        selectShippingAddress,
        selectShippingMethodAction,
        paymentService,
        selectPaymentMethodAction,
        addressConverter,
        selectBillingAddress,
        createBillingAddress,
        _
    ) {
        'use strict';

        let isBillingAddressResolvedFromBackend = false;

        return {
            /**
             * @param {Object} ratesData
             */
            resolveShippingRates: function (ratesData) {
                console.log(cartData.items);
                let selectedShippingRate = checkoutData.getSelectedShippingRate(),
                    availableRate = false;

                if (ratesData.length === 1 && !quote.shippingMethod()) {
                    //set shipping rate if we have only one available shipping rate
                    selectShippingMethodAction(ratesData[0]);
                    return;
                }

                if (quote.shippingMethod()) {
                    availableRate = _.find(ratesData, function (rate) {
                        return rate['carrier_code'] === quote.shippingMethod()['carrier_code'] &&
                            rate['method_code'] === quote.shippingMethod()['method_code'];
                    });
                }

                if (!availableRate && selectedShippingRate) {
                    availableRate = _.find(ratesData, function (rate) {
                        return rate['carrier_code'] + '_' + rate['method_code'] === selectedShippingRate;
                    });
                }

                if (!availableRate && window.checkoutConfig.selectedShippingMethod) {
                    availableRate = _.find(ratesData, function (rate) {
                        let selectedShippingMethod = window.checkoutConfig.selectedShippingMethod;

                        return rate['carrier_code'] === selectedShippingMethod['carrier_code'] &&
                            rate['method_code'] === selectedShippingMethod['method_code'];
                    });
                }

                if (!availableRate) {
                    selectShippingMethodAction(null);
                } else {
                    selectShippingMethodAction(availableRate);
                }
            },
        };
    }
);
