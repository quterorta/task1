define([
    'jquery',
    'mage/utils/wrapper',
    'Magento_Checkout/js/model/quote'
], function ($, wrapper,quote) {
    'use strict';

    return function (setShippingInformationAction) {
        return wrapper.wrap(setShippingInformationAction, function (originalAction) {
            var shippingAddress = quote.shippingAddress();
            if (shippingAddress['extension_attributes'] === undefined) {
                shippingAddress['extension_attributes'] = {};
            }

             $.each(shippingAddress.customAttributes, function(index, eachCustomAttribute){
                if(eachCustomAttribute.attribute_code === 'return_address')
                {
                    shippingAddress['extension_attributes']['return_address'] = eachCustomAttribute.value ;
                }
            });

            return originalAction();
        });
    };
});
