define(
    [
        'jquery',
        'Magento_Checkout/js/cart-data'
    ],
    function ($, cartData) {
        'use strict';

        return function (target) {
            return function (shippingMethod) {
                target(shippingMethod);
            };
        }
    }
);
