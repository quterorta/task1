<?php

namespace Shipping\International\Observer\QuoteItemValidator;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Shipping\International\Service\CustomCartValidator;

class WeightValidatorObserver implements ObserverInterface
{
    /**
     * @var CustomCartValidator
     */
    private $customCartValidator;

    /**
     * @param CustomCartValidator $customCartValidator
     */
    public function __construct(
        CustomCartValidator $customCartValidator
    ) {
        $this->customCartValidator = $customCartValidator;
    }

    /**
     * @param EventObserver $observer
     * @return void
     */
    public function execute(EventObserver $observer)
    {
        $quoteItem = $observer->getEvent()->getItem();
        if ($this->customCartValidator->checkQuote($quoteItem->getQuote()) &&
            !$this->customCartValidator->validateQuoteItemWeight($quoteItem)
        ) {
            $this->customCartValidator->setErrorForQuoteItem($quoteItem);
        }
    }
}
