<?php

namespace Shipping\International\Observer\Checkout;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Shipping\International\Model\Carrier\Shipping;
use Shipping\International\Model\Payment\CreditCardPaymentMethod;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;

/**
 * Plugin for show/hide custom payment method
 */
class MethodIsActive implements ObserverInterface
{
    /**
     * @var Cart
     */
    protected $_cart;
    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * @param Cart $cart
     * @param Session $checkoutSession
     */
    public function __construct(
        Cart $cart,
        Session $checkoutSession
    )
    {
        $this->_cart = $cart;
        $this->_checkoutSession = $checkoutSession;
    }
    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try {
            $quote = $this->getCheckoutSession()->getQuote();

            $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
            $paymentMethodCode = $observer->getEvent()->getMethodInstance()->getCode();

            if ($shippingMethod === Shipping::FULL_CODE_SHIPPING_INTERNATIONAL_METHOD) {
                $checkResult = $observer->getEvent()->getResult();
                if ($paymentMethodCode != CreditCardPaymentMethod::PAYMENT_METHOD_CODE) {
                    $checkResult->setData('is_available', false);
                }
            }
        } catch (NoSuchEntityException|LocalizedException $e) {
            return;
        }
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->_cart;
    }

    /**
     * @return Session
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}
