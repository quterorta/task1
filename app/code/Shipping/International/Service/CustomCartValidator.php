<?php

namespace Shipping\International\Service;

use Magento\CatalogInventory\Helper\Data;
use Magento\Quote\Model\Quote\Item as QuoteItem;
use Magento\Quote\Model\Quote;

class CustomCartValidator
{
    private const SHIPPING_INTERNATIONAL = 'international_shipping_international_shipping';

    /**
     * @param Quote $quote
     * @return bool
     */
    public function checkQuote(Quote $quote): bool
    {
        if ($quote->getIsSuperMode()) {
            return false;
        }

        $shippingAddress = $quote->getShippingAddress();
        if (!$shippingAddress) {
            return false;
        }

        $shippingMethod = $shippingAddress->getShippingMethod();
        if (!$shippingMethod || $shippingMethod !== self::SHIPPING_INTERNATIONAL) {
            return false;
        }

        return true;
    }

    /**
     * @param QuoteItem $quoteItem
     * @return bool
     */
    public function validateQuoteItemWeight(QuoteItem $quoteItem): bool
    {
        $validationResult = true;

        $product = $quoteItem->getProduct();
        $productWeight = $product->getWeight();
        if (!$productWeight || $productWeight == 0) {
            $validationResult = false;
        }

        return $validationResult;
    }

    /**
     * @param QuoteItem $quoteItem
     * @return void
     */
    public function setErrorForQuoteItem(QuoteItem $quoteItem)
    {
        if (!$this->validateQuoteItemWeight($quoteItem)) {
            $quoteItem->addErrorInfo(
                'cataloginventory',
                Data::ERROR_QTY,
                __('This product does not have weight, please remove this item from your cart.')
            );

            $quoteItem->getQuote()->addErrorInfo(
                'stock',
                'cataloginventory',
                Data::ERROR_QTY,
                __('Some of the products does not have weight, please remove this items from your cart.')
            );
        } else {
            $this->removeErrorsFromQuoteAndItem($quoteItem, Data::ERROR_QTY);
        }
        return;
    }

    /**
     * @param QuoteItem $item
     * @param string $errorCode
     * @return void
     */
    public function removeErrorsFromQuoteAndItem(QuoteItem $item, string $errorCode)
    {
        if ($item->getHasError()) {
            $params = ['origin' => 'cataloginventory', 'code' => $errorCode];
            $item->removeErrorInfosByParams($params);
        }

        $quote = $item->getQuote();
        if ($quote->getHasError()) {
            $quoteItems = $quote->getItemsCollection();
            $canRemoveError = true;
            foreach ($quoteItems as $quoteItem) {
                if ($quoteItem->getItemId() == $item->getItemId()) {
                    continue;
                }

                $errorInfos = $quoteItem->getErrorInfos();
                foreach ($errorInfos as $errorInfo) {
                    if ($errorInfo['code'] == $errorCode) {
                        $canRemoveError = false;
                        break;
                    }
                }

                if (!$canRemoveError) {
                    break;
                }
            }

            if ($canRemoveError) {
                $params = ['origin' => 'cataloginventory', 'code' => $errorCode];
                //@phpstan-ignore-next-line
                $quote->removeErrorInfosByParams(null, $params);
            }
        }
    }
}
