<?php

namespace Shipping\International\Plugin;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Checkout\Api\Data\ShippingInformationInterface;
use Magento\Checkout\Model\ShippingInformationManagement;
use Psr\Log\LoggerInterface;

class ShippingInformationManagementPlugin
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @param LoggerInterface $logger
     * @param CartRepositoryInterface $quoteRepository
     */
    public function __construct(
        LoggerInterface $logger,
        CartRepositoryInterface $quoteRepository
    ) {
        $this->logger = $logger;
        $this->quoteRepository = $quoteRepository;
    }

    /**
     * @param ShippingInformationManagement $subject
     * @param int $cartId
     * @param ShippingInformationInterface $addressInformation
     * @return array
     * @throws Exception
     */
    public function beforeSaveAddressInformation(
        ShippingInformationManagement $subject,
        int $cartId,
        ShippingInformationInterface $addressInformation
    ): array {
        try {
            $quote = $this->quoteRepository->getActive($cartId);
            $returnAddress = $addressInformation->getShippingAddress()->getExtensionAttributes()->getReturnAddress();
            $quote->setReturnAddress($returnAddress);

            $quote->save();
        } catch (NoSuchEntityException $e) {
            $this->logger->error($e->getLogMessage());
        }

        return [$cartId, $addressInformation];
    }
}
