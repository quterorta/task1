<?php

declare(strict_types=1);

namespace Shipping\International\Plugin\Checkout;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\OfflinePayments\Model\Cashondelivery;
use Shipping\International\Model\Carrier\Shipping;

class HidePaymentMethod
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        CheckoutSession $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }


    /**
     * @param Cashondelivery $cashondelivery
     * @param $result
     * @return false|mixed
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function afterCanUseCheckout(Cashondelivery $cashondelivery, $result)
    {
        $quote = $this->checkoutSession->getQuote();

        $shippingMethod = $quote->getShippingAddress()->getShippingMethod();
        if ($shippingMethod === Shipping::CODE_CUSTOM_SHIPPING_PAYMENT_MODULE) {
            return false;
        }
        return $result;
    }
}
