<?php

namespace Shipping\International\Plugin\Checkout;

use Magento\Checkout\Block\Checkout\LayoutProcessor;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\AddressFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Template\Context;

class LayoutProcessorPlugin
{

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Session
     */
    protected $checkoutSession;

    /**
     * @var AddressFactory
     */
    protected $customerAddressFactory;

    /**
     * @var FormKey
     */
    protected $formKey;

    public function __construct(
        Context           $context,
        Session           $checkoutSession,
        AddressFactory    $customerAddressFactory
    )
    {
        $this->scopeConfig = $context->getScopeConfig();
        $this->checkoutSession = $checkoutSession;
        $this->customerAddressFactory = $customerAddressFactory;
    }

    /**
     * @param LayoutProcessor $subject
     * @param array $jsLayout
     * @return array
     */
    public function afterProcess(
        LayoutProcessor $subject,
        array           $jsLayout
    ): array
    {
        $validation['required-entry'] = false;
        $customField = [
            'component' => "Magento_Ui/js/form/element/abstract",
            'config' => [
                'customScope' => 'customShippingMethodFields',
                'template' => 'ui/form/field',
                'elementTmpl' => "ui/form/element/input",
                'tooltip' => [
                    'description' => 'Return address for international shipping method',
                ],
                'id' => "return_address"
            ],
            'dataScope' => 'shippingAddress.extension_attributes.return_address',
            'label' => "Return Address",
            'provider' => 'checkoutProvider',
            'visible' => true,
            'validation' => $validation,
            'sortOrder' => 2,
            'id' => 'custom_shipping_field[return_address]'
        ];

        $jsLayout['components']['checkout']['children']
        ['steps']['children']['shipping-step']
        ['children']['shippingAddress']['children']
        ['custom-shipping-method-fields']['children']['return_address'] = $customField;

        return $jsLayout;
    }
}
