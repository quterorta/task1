<?php

namespace Shipping\International\Model\Carrier;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Quote\Model\Quote\Item;
use Psr\Log\LoggerInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\ResultFactory;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\CatalogInventory\Helper\Data;

class Shipping extends AbstractCarrier implements CarrierInterface
{
    protected $_code = self::CODE_CUSTOM_SHIPPING_PAYMENT_MODULE;

    public const CODE_CUSTOM_SHIPPING_PAYMENT_MODULE = 'international_shipping';
    public const FULL_CODE_SHIPPING_INTERNATIONAL_METHOD = 'international_shipping_international_shipping';

    /** @var ResultFactory */
    protected ResultFactory $rateResultFactory;

    /* @var MethodFactory */
    protected MethodFactory $rateMethodFactory;

    /**
     * @var CheckoutSession
     */
    private CheckoutSession $checkoutSession;

    /**
     * Shipping constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param ErrorFactory $rateErrorFactory
     * @param LoggerInterface $logger
     * @param ResultFactory $rateResultFactory
     * @param MethodFactory $rateMethodFactory
     * @param CheckoutSession $checkoutSession
     * @param array $data
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        CheckoutSession $checkoutSession,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->checkoutSession = $checkoutSession;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * @return false|Item[]
     */
    public function getQuoteItems()
    {
        try {
            $quote = $this->checkoutSession->getQuote();
            return $quote->getAllVisibleItems();
        } catch (NoSuchEntityException|LocalizedException $e) {
            return false;
        }
    }

    /**
     * @return float|int
     */
    private function getShippingPrice()
    {
        $configPrice = $this->getConfigData('price');

        $shippingPrice = $this->getFinalPriceWithHandlingFee($configPrice);

        $quoteItems = $this->getQuoteItems();
        $totalWeight = 0;
        if (!$quoteItems) {
            return $shippingPrice;
        }

        foreach ($quoteItems as $quoteItem) {
            $weight = $quoteItem->getProduct()->getWeight();
            $totalWeight = $totalWeight + $weight;
        }
        return 700 + 280*$totalWeight;
    }

    /**
     * @param RateRequest $request
     * @return false|Result
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        /** @var Result $result */
        $result = $this->rateResultFactory->create();

        /** @var Method $method */
        $method = $this->rateMethodFactory->create();

        $method->setCarrier($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod($this->_code);
        $method->setMethodTitle($this->getConfigData('name'));

        $amount = $this->getShippingPrice();



        $method->setPrice($amount);
        $method->setCost($amount);

        $result->append($method);

        $itemHasWeight = true;

        $quoteItems = $this->getQuoteItems();
        if (!$quoteItems) {
            return false;
        }

        foreach ($quoteItems as $quoteItem) {
            $quoteItemWeight = $quoteItem->getProduct()->getWeight();
            if (!$quoteItemWeight || $quoteItemWeight == 0) {
                $itemHasWeight = false;
                $quoteItem->addErrorInfo(
                    'cataloginventory',
                    Data::ERROR_QTY,
                    __('This product does not have weight, please remove this item from your cart.')
                );
            }
        }

        return $result;
    }
}
