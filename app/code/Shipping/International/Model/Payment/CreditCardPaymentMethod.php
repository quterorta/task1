<?php

namespace Shipping\International\Model\Payment;

use Magento\Payment\Model\Method\AbstractMethod;

/**
 * Pay In Store payment method model
 */
class CreditCardPaymentMethod extends AbstractMethod
{
    public const PAYMENT_METHOD_CODE = 'shipping_international_credit_card_payment';
    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = self::PAYMENT_METHOD_CODE;
}
